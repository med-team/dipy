From: Serge Koudoro <skab12@gmail.com>
Date: Tue, 21 Jan 2025 19:10:31 -0500
Subject: RF: create c version of multiple methods.

Origin: upstream, https://github.com/dipy/dipy/commit/b410a28759bcb9ac9edf2bcc6dca08b8e40cc7e2
Forwarded: not-needed

This will allow the use of nogil in the following methods:

- peak_directions_c
- local_maxima_c
- search_descending_c
- remove_similar_vertices_c

NF: Add new module named math in dipy/core
---
 dipy/core/math.pxd                   |  23 ++++
 dipy/core/math.pyx                   |  29 +++++
 dipy/core/meson.build                |  10 +-
 dipy/core/tests/meson.build          |  18 +++
 dipy/core/tests/test_math.pyx        |  38 ++++++
 dipy/direction/peaks.py              |  81 +-----------
 dipy/reconst/dirspeed.pyx            | 223 +++++++++++++++++++++++++++++++++
 dipy/reconst/meson.build             |   7 +-
 dipy/reconst/recspeed.pxd            |  27 ++++
 dipy/reconst/recspeed.pyx            | 234 ++++++++++++++++++++++++-----------
 dipy/utils/fast_numpy.pxd            |   7 ++
 dipy/utils/fast_numpy.pyx            |  25 ++++
 dipy/utils/tests/test_fast_numpy.pyx |  13 +-
 13 files changed, 580 insertions(+), 155 deletions(-)
 create mode 100644 dipy/core/math.pxd
 create mode 100644 dipy/core/math.pyx
 create mode 100644 dipy/core/tests/test_math.pyx
 create mode 100644 dipy/reconst/dirspeed.pyx
 create mode 100644 dipy/reconst/recspeed.pxd

diff --git a/dipy/core/math.pxd b/dipy/core/math.pxd
new file mode 100644
index 0000000..6bf7930
--- /dev/null
+++ b/dipy/core/math.pxd
@@ -0,0 +1,23 @@
+# cython: boundscheck=False
+# cython: cdivision=True
+# cython: initializedcheck=False
+# cython: wraparound=False
+# cython: nonecheck=False
+# cython: overflowcheck=False
+
+
+from dipy.align.fused_types cimport floating
+
+
+cdef inline floating f_max(floating a, floating b) noexcept nogil:
+    """Return the maximum of a and b."""
+    return a if a >= b else b
+
+
+cdef inline floating f_min(floating a, floating b) noexcept nogil:
+    """Return the minimum of a and b."""
+    return a if a <= b else b
+
+
+cdef floating f_array_min(floating* arr, int n) noexcept nogil
+cdef floating f_array_max(floating* arr, int n) noexcept nogil
diff --git a/dipy/core/math.pyx b/dipy/core/math.pyx
new file mode 100644
index 0000000..a47f338
--- /dev/null
+++ b/dipy/core/math.pyx
@@ -0,0 +1,29 @@
+# cython: boundscheck=False
+# cython: cdivision=True
+# cython: initializedcheck=False
+# cython: wraparound=False
+# cython: nonecheck=False
+# cython: overflowcheck=False
+
+
+from dipy.align.fused_types cimport floating
+
+
+cdef floating f_array_min(floating* arr, int n) noexcept nogil:
+    """Return the minimum value of an array."""
+    cdef int i
+    cdef double min_val = arr[0]
+    for i in range(1, n):
+        if arr[i] < min_val:
+            min_val = arr[i]
+    return min_val
+
+
+cdef floating f_array_max(floating* arr, int n) noexcept nogil:
+    """Compute the maximum value of an array."""
+    cdef int i
+    cdef double max_val = arr[0]
+    for i in range(1, n):
+        if arr[i] > max_val:
+            max_val = arr[i]
+    return max_val
\ No newline at end of file
diff --git a/dipy/core/meson.build b/dipy/core/meson.build
index b4e9836..53c5b9d 100644
--- a/dipy/core/meson.build
+++ b/dipy/core/meson.build
@@ -1,6 +1,12 @@
-cython_sources = ['interpolation',]
+cython_sources = [
+  'interpolation',
+  'math',
+]
 
-cython_headers = ['interpolation.pxd',]
+cython_headers = [
+  'interpolation.pxd',
+  'math.pxd',
+]
 
 foreach ext: cython_sources
   if fs.exists(ext + '.pxd')
diff --git a/dipy/core/tests/meson.build b/dipy/core/tests/meson.build
index edbc8c6..caee0f1 100644
--- a/dipy/core/tests/meson.build
+++ b/dipy/core/tests/meson.build
@@ -1,3 +1,21 @@
+cython_sources = [
+  'test_math',
+  ]
+
+foreach ext: cython_sources
+  if fs.exists(ext + '.pxd')
+    extra_args += ['--depfile', meson.current_source_dir() +'/'+ ext + '.pxd', ]
+  endif
+  py3.extension_module(ext,
+    cython_gen.process(ext + '.pyx'),
+    c_args: cython_c_args,
+    include_directories: [incdir_numpy, inc_local],
+    dependencies: [omp],
+    install: true,
+    subdir: 'dipy/core/tests'
+  )
+endforeach
+
 python_sources = ['__init__.py',
   'test_geometry.py',
   'test_gradients.py',
diff --git a/dipy/core/tests/test_math.pyx b/dipy/core/tests/test_math.pyx
new file mode 100644
index 0000000..8744f3d
--- /dev/null
+++ b/dipy/core/tests/test_math.pyx
@@ -0,0 +1,38 @@
+
+
+cimport numpy as cnp
+import numpy as np
+import numpy.testing as npt
+
+from dipy.core.math cimport f_array_min, f_array_max, f_max, f_min
+
+
+def test_f_array_max():
+    cdef int size = 10
+    cdef double[:] d_arr = np.arange(size, dtype=np.float64)
+    cdef float[:] f_arr = np.arange(size, dtype=np.float32)
+
+    npt.assert_equal(f_array_max(&d_arr[0], size), 9)
+    npt.assert_equal(f_array_max(&f_arr[0], size), 9)
+
+
+def test_f_array_min():
+    cdef int size = 6
+    cdef double[:] d_arr = np.arange(5, 5+size, dtype=np.float64)
+    cdef float[:] f_arr = np.arange(5, 5+size, dtype=np.float32)
+
+    npt.assert_almost_equal(f_array_min(&d_arr[0], size), 5)
+    npt.assert_almost_equal(f_array_min(&f_arr[0], size), 5)
+
+
+def test_f_max():
+    npt.assert_equal(f_max[double](1, 2), 2)
+    npt.assert_equal(f_max[float](2, 1), 2)
+    npt.assert_equal(f_max(1., 1.), 1)
+
+
+def test_f_min():
+    npt.assert_equal(f_min[double](1, 2), 1)
+    npt.assert_equal(f_min[float](2, 1), 1)
+    npt.assert_equal(f_min(1.0, 1.0), 1)
+
diff --git a/dipy/direction/peaks.py b/dipy/direction/peaks.py
index 1da3984..655b0cd 100644
--- a/dipy/direction/peaks.py
+++ b/dipy/direction/peaks.py
@@ -10,6 +10,7 @@ from dipy.core.interpolation import trilinear_interpolate4d
 from dipy.core.ndindex import ndindex
 from dipy.core.sphere import Sphere
 from dipy.data import default_sphere
+from dipy.reconst.dirspeed import peak_directions
 from dipy.reconst.eudx_direction_getter import EuDXDirectionGetter
 from dipy.reconst.odf import gfa
 from dipy.reconst.recspeed import (
@@ -98,86 +99,6 @@ def peak_directions_nl(
     return directions, values
 
 
-@warning_for_keywords()
-def peak_directions(
-    odf,
-    sphere,
-    *,
-    relative_peak_threshold=0.5,
-    min_separation_angle=25,
-    is_symmetric=True,
-):
-    """Get the directions of odf peaks.
-
-    Peaks are defined as points on the odf that are greater than at least one
-    neighbor and greater than or equal to all neighbors. Peaks are sorted in
-    descending order by their values then filtered based on their relative size
-    and spacing on the sphere. An odf may have 0 peaks, for example if the odf
-    is perfectly isotropic.
-
-    Parameters
-    ----------
-    odf : 1d ndarray
-        The odf function evaluated on the vertices of `sphere`
-    sphere : Sphere
-        The Sphere providing discrete directions for evaluation.
-    relative_peak_threshold : float in [0., 1.]
-        Only peaks greater than ``min + relative_peak_threshold * scale`` are
-        kept, where ``min = max(0, odf.min())`` and
-        ``scale = odf.max() - min``.
-    min_separation_angle : float in [0, 90]
-        The minimum distance between directions. If two peaks are too close
-        only the larger of the two is returned.
-    is_symmetric : bool, optional
-        If True, v is considered equal to -v.
-
-    Returns
-    -------
-    directions : (N, 3) ndarray
-        N vertices for sphere, one for each peak
-    values : (N,) ndarray
-        peak values
-    indices : (N,) ndarray
-        peak indices of the directions on the sphere
-
-    Notes
-    -----
-    If the odf has any negative values, they will be clipped to zeros.
-
-    """
-    values, indices = local_maxima(odf, sphere.edges)
-
-    # If there is only one peak return
-    n = len(values)
-    if n == 0 or (values[0] < 0.0):
-        return np.zeros((0, 3)), np.zeros(0), np.zeros(0, dtype=int)
-    elif n == 1:
-        return sphere.vertices[indices], values, indices
-
-    odf_min = np.min(odf)
-    odf_min = max(odf_min, 0.0)
-    # because of the relative threshold this algorithm will give the same peaks
-    # as if we divide (values - odf_min) with (odf_max - odf_min) or not so
-    # here we skip the division to increase speed
-    values_norm = values - odf_min
-
-    # Remove small peaks
-    n = search_descending(values_norm, relative_peak_threshold)
-    indices = indices[:n]
-    directions = sphere.vertices[indices]
-
-    # Remove peaks too close together
-    directions, uniq = remove_similar_vertices(
-        directions,
-        min_separation_angle,
-        return_index=True,
-        remove_antipodal=is_symmetric,
-    )
-    values = values[uniq]
-    indices = indices[uniq]
-    return directions, values, indices
-
-
 def _pam_from_attrs(
     klass, sphere, peak_indices, peak_values, peak_dirs, gfa, qa, shm_coeff, B, odf
 ):
diff --git a/dipy/reconst/dirspeed.pyx b/dipy/reconst/dirspeed.pyx
new file mode 100644
index 0000000..35c3112
--- /dev/null
+++ b/dipy/reconst/dirspeed.pyx
@@ -0,0 +1,223 @@
+# cython: boundscheck=False
+# cython: cdivision=True
+# cython: initializedcheck=False
+# cython: wraparound=False
+# cython: nonecheck=False
+# cython: overflowcheck=False
+
+
+import numpy as np
+cimport numpy as cnp
+
+import cython
+from libc.stdlib cimport malloc, free
+from libc.string cimport memcpy
+from libc.math cimport cos, M_PI, INFINITY
+
+from dipy.core.math cimport f_max, f_array_min
+from dipy.reconst.recspeed cimport local_maxima_c, search_descending_c, remove_similar_vertices_c
+
+
+cdef cnp.uint16_t peak_directions_c(
+    double[:] odf,
+    double[:, ::1] sphere_vertices,
+    cnp.uint16_t[:, ::1] sphere_edges,
+    double relative_peak_threshold,
+    double min_separation_angle,
+    bint is_symmetric,
+    double[:, ::1] out_directions,
+    double[::1] out_values,
+    cnp.npy_intp[::1] out_indices,
+    double[:, :] unique_vertices,
+    cnp.uint16_t[:] mapping,
+    cnp.uint16_t[:] index
+) noexcept nogil:
+    """Get the directions of odf peaks.
+
+    Peaks are defined as points on the odf that are greater than at least one
+    neighbor and greater than or equal to all neighbors. Peaks are sorted in
+    descending order by their values then filtered based on their relative size
+    and spacing on the sphere. An odf may have 0 peaks, for example if the odf
+    is perfectly isotropic.
+
+    Parameters
+    ----------
+    odf : 1d ndarray
+        The odf function evaluated on the vertices of `sphere`
+    sphere_vertices : (N,3) ndarray
+        The sphere vertices providing discrete directions for evaluation.
+    sphere_edges : (N, 2) ndarray
+
+    relative_peak_threshold : float in [0., 1.]
+        Only peaks greater than ``min + relative_peak_threshold * scale`` are
+        kept, where ``min = max(0, odf.min())`` and
+        ``scale = odf.max() - min``.
+    min_separation_angle : float in [0, 90]
+        The minimum distance between directions. If two peaks are too close
+        only the larger of the two is returned.
+    is_symmetric : bool, optional
+        If True, v is considered equal to -v.
+    out_directions : (N, 3) ndarray
+        The directions of the peaks, N vertices for sphere, one for each peak
+    out_values : (N,) ndarray
+        The peak values
+    out_indices: (N,) ndarray
+        The peak indices of the directions on the sphere
+    unique_vertices : (N, 3) ndarray
+        The unique vertices
+    mapping  : (N,) ndarray
+        For each element ``vertices[i]`` ($i \in 0..N-1$), the index $j$ to a
+        vertex in `unique_vertices` that is less than `theta` degrees from
+        ``vertices[i]``.
+    index : (N,) ndarray
+        `index` gives the reverse of `mapping`.  For each element
+        ``unique_vertices[j]`` ($j \in 0..M-1$), the index $i$ to a vertex in
+        `vertices` that is less than `theta` degrees from
+        ``unique_vertices[j]``.  If there is more than one element of
+        `vertices` that is less than theta degrees from `unique_vertices[j]`,
+        return the first (lowest index) matching value.
+    Returns
+    -------
+    n_unique : int
+        The number of unique peaks
+
+    Notes
+    -----
+    If the odf has any negative values, they will be clipped to zeros.
+
+    """
+    cdef cnp.npy_intp i, n, idx
+    cdef long count
+    cdef double odf_min
+    cdef cnp.npy_intp num_vertices = sphere_vertices.shape[0]
+    cdef double* tmp_buffer
+    cdef cnp.uint16_t n_unique = 0
+
+    count = local_maxima_c(odf, sphere_edges, out_values, out_indices)
+
+    # If there is only one peak return
+    if count == 0:
+        return 0
+    elif count == 1:
+        return 1
+
+    odf_min = f_array_min[double](&odf[0], num_vertices)
+    odf_min = f_max[double](odf_min, 0.0)
+
+    # because of the relative threshold this algorithm will give the same peaks
+    # as if we divide (values - odf_min) with (odf_max - odf_min) or not so
+    # here we skip the division to increase speed
+    tmp_buffer = <double*>malloc(count * sizeof(double))
+    memcpy(tmp_buffer, &out_values[0], count * sizeof(double))
+
+    for i in range(count):
+        tmp_buffer[i] -= odf_min
+
+    # Remove small peaks
+    n = search_descending_c[cython.double](tmp_buffer, <cnp.npy_intp>count, relative_peak_threshold)
+
+    for i in range(n):
+        idx = out_indices[i]
+        out_directions[i, :] = sphere_vertices[idx, :]
+
+    # Remove peaks too close together
+    remove_similar_vertices_c(
+        out_directions[:n, :],
+        min_separation_angle,
+        is_symmetric,
+        0,  # return_mapping
+        1,  # return_index
+        unique_vertices[:n, :],
+        mapping[:n],
+        index[:n],
+        &n_unique
+    )
+
+    # Update final results
+    for i in range(n_unique):
+        idx = index[i]
+        out_directions[i, :] = unique_vertices[i, :]
+        out_values[i] = out_values[idx]
+        out_indices[i] = out_indices[idx]
+
+    free(tmp_buffer)
+    return n_unique
+
+
+def peak_directions(
+    double[:] odf,
+    sphere,
+    *,
+    relative_peak_threshold=0.5,
+    min_separation_angle=25,
+    bint is_symmetric=True,
+):
+    """Get the directions of odf peaks.
+
+    Peaks are defined as points on the odf that are greater than at least one
+    neighbor and greater than or equal to all neighbors. Peaks are sorted in
+    descending order by their values then filtered based on their relative size
+    and spacing on the sphere. An odf may have 0 peaks, for example if the odf
+    is perfectly isotropic.
+
+    Parameters
+    ----------
+    odf : 1d ndarray
+        The odf function evaluated on the vertices of `sphere`
+    sphere : Sphere
+        The Sphere providing discrete directions for evaluation.
+    relative_peak_threshold : float in [0., 1.]
+        Only peaks greater than ``min + relative_peak_threshold * scale`` are
+        kept, where ``min = max(0, odf.min())`` and
+        ``scale = odf.max() - min``.
+    min_separation_angle : float in [0, 90]
+        The minimum distance between directions. If two peaks are too close
+        only the larger of the two is returned.
+    is_symmetric : bool, optional
+        If True, v is considered equal to -v.
+
+    Returns
+    -------
+    directions : (N, 3) ndarray
+        N vertices for sphere, one for each peak
+    values : (N,) ndarray
+        peak values
+    indices : (N,) ndarray
+        peak indices of the directions on the sphere
+
+    Notes
+    -----
+    If the odf has any negative values, they will be clipped to zeros.
+
+    """
+    cdef double[:, ::1] vertices = sphere.vertices
+    cdef cnp.uint16_t[:, ::1] edges = sphere.edges
+
+    cdef cnp.npy_intp num_vertices = sphere.vertices.shape[0]
+    cdef cnp.uint16_t n_unique = 0
+
+    cdef double[:, ::1] directions_out = np.zeros((num_vertices, 3), dtype=np.float64)
+    cdef double[::1] values_out = np.zeros(num_vertices, dtype=np.float64)
+    cdef cnp.npy_intp[::1] indices_out = np.zeros(num_vertices, dtype=np.intp)
+    cdef cnp.float64_t[:, ::1] unique_vertices = np.empty((num_vertices, 3), dtype=np.float64)
+    cdef cnp.uint16_t[::1] mapping = None
+    cdef cnp.uint16_t[::1] index = np.empty(num_vertices, dtype=np.uint16)
+
+    n_unique = peak_directions_c(
+        odf, vertices, edges, relative_peak_threshold, min_separation_angle, is_symmetric,
+        directions_out, values_out, indices_out, unique_vertices, mapping, index
+    )
+
+    if n_unique == 0:
+        return np.zeros((0, 3)), np.zeros(0), np.zeros(0, dtype=int)
+    elif n_unique == 1:
+        return (
+            sphere.vertices[np.asarray(indices_out[:n_unique]).astype(int)],
+            np.asarray(values_out[:n_unique]),
+            np.asarray(indices_out[:n_unique]),
+        )
+    return (
+        np.asarray(directions_out[:n_unique, :]),
+        np.asarray(values_out[:n_unique]),
+        np.asarray(indices_out[:n_unique]),
+    )
\ No newline at end of file
diff --git a/dipy/reconst/meson.build b/dipy/reconst/meson.build
index 0f8040d..ae40990 100644
--- a/dipy/reconst/meson.build
+++ b/dipy/reconst/meson.build
@@ -1,10 +1,15 @@
 cython_sources = [
+  'dirspeed',
   'eudx_direction_getter',
   'quick_squash',
   'recspeed',
   'vec_val_sum',
   ]
 
+cython_headers = [
+  'recspeed.pxd',
+  ]
+
 foreach ext: cython_sources
   py3.extension_module(ext,
     cython_gen.process(ext + '.pyx'),
@@ -48,7 +53,7 @@ python_sources = ['__init__.py',
   ]
 
 py3.install_sources(
-  python_sources,
+  python_sources + cython_headers,
   pure: false,
   subdir: 'dipy/reconst'
 )
diff --git a/dipy/reconst/recspeed.pxd b/dipy/reconst/recspeed.pxd
new file mode 100644
index 0000000..48d12a4
--- /dev/null
+++ b/dipy/reconst/recspeed.pxd
@@ -0,0 +1,27 @@
+cimport cython
+cimport numpy as cnp
+
+
+cdef void remove_similar_vertices_c(
+    double[:, :] vertices,
+    double theta,
+    int remove_antipodal,
+    int return_mapping,
+    int return_index,
+    double[:, :] unique_vertices,
+    cnp.uint16_t[:] mapping,
+    cnp.uint16_t[:] index,
+    cnp.uint16_t* n_unique
+) noexcept nogil
+
+
+cdef cnp.npy_intp search_descending_c(
+    cython.floating* arr,
+    cnp.npy_intp size,
+    double relative_threshold) noexcept nogil
+
+
+cdef long local_maxima_c(
+    double[:] odf, cnp.uint16_t[:, :] edges,
+    double[::1] out_values,
+    cnp.npy_intp[::1] out_indices) noexcept nogil
\ No newline at end of file
diff --git a/dipy/reconst/recspeed.pyx b/dipy/reconst/recspeed.pyx
index cceccf4..db0b0cf 100644
--- a/dipy/reconst/recspeed.pyx
+++ b/dipy/reconst/recspeed.pyx
@@ -10,6 +10,12 @@ cimport cython
 import numpy as np
 cimport numpy as cnp
 
+from libc.math cimport cos, fabs, M_PI
+from libc.stdlib cimport malloc, free
+from libc.string cimport memset, memcpy
+
+from dipy.utils.fast_numpy cimport take
+
 cdef extern from "dpy_math.h" nogil:
     double floor(double x)
     double fabs(double x)
@@ -33,12 +39,63 @@ cdef inline double* asdp(cnp.ndarray pt):
 
 @cython.boundscheck(False)
 @cython.wraparound(False)
+cdef void remove_similar_vertices_c(
+    double[:, :] vertices,
+    double theta,
+    int remove_antipodal,
+    int return_mapping,
+    int return_index,
+    double[:, :] unique_vertices,
+    cnp.uint16_t[:] mapping,
+    cnp.uint16_t[:] index,
+    cnp.uint16_t* n_unique
+) noexcept nogil:
+    """
+    Optimized Cython version to remove vertices that are less than `theta` degrees from any other.
+    """
+    cdef:
+        int n = vertices.shape[0]
+        int i, j
+        int pass_all
+        double a, b, c, sim, cos_similarity
+
+    cos_similarity = cos(M_PI / 180 * theta)
+    n_unique[0] = 0
+
+    for i in range(n):
+        pass_all = 1
+        a = vertices[i, 0]
+        b = vertices[i, 1]
+        c = vertices[i, 2]
+        for j in range(n_unique[0]):
+            sim = (a * unique_vertices[j, 0] +
+                   b * unique_vertices[j, 1] +
+                   c * unique_vertices[j, 2])
+            if remove_antipodal:
+                sim = fabs(sim)
+            if sim > cos_similarity:
+                pass_all = 0
+                if return_mapping:
+                    mapping[i] = j
+                break
+        if pass_all:
+            unique_vertices[n_unique[0], 0] = a
+            unique_vertices[n_unique[0], 1] = b
+            unique_vertices[n_unique[0], 2] = c
+            if return_mapping:
+                mapping[i] = n_unique[0]
+            if return_index:
+                index[n_unique[0]] = i
+            n_unique[0] += 1
+
+
 def remove_similar_vertices(
-    cython.floating[:, :] vertices,
+    cnp.float64_t[:, ::1] vertices,
     double theta,
     bint return_mapping=False,
     bint return_index=False,
-    bint remove_antipodal=True):
+    bint remove_antipodal=True
+):
     """Remove vertices that are less than `theta` degrees from any other
 
     Returns vertices that are at least theta degrees from any other vertex.
@@ -82,66 +139,83 @@ def remove_similar_vertices(
     """
     if vertices.shape[1] != 3:
         raise ValueError('Vertices should be 2D with second dim length 3')
-    cdef:
-        cnp.ndarray[cnp.float_t, ndim=2, mode='c'] unique_vertices
-        cnp.ndarray[cnp.uint16_t, ndim=1, mode='c'] mapping
-        cnp.ndarray[cnp.uint16_t, ndim=1, mode='c'] index
-        char pass_all
-        # Variable has to be large enough for all valid sizes of vertices
-        cnp.npy_int32 i, j
-        cnp.npy_int32 n_unique = 0
-        # Large enough for all possible sizes of vertices
-        cnp.npy_intp n = vertices.shape[0]
-        double a, b, c, sim
-        double cos_similarity = cos(DPY_PI/180 * theta)
-    if n >= 2**16:  # constrained by input data type
-        raise ValueError("too many vertices")
-    unique_vertices = np.empty((n, 3), dtype=float)
+
+    cdef int n = vertices.shape[0]
+    if n >= 2**16:
+        raise ValueError("Too many vertices")
+
+    cdef cnp.float64_t[:, ::1] unique_vertices = np.empty((n, 3), dtype=np.float64)
+    cdef cnp.uint16_t[::1] mapping = None
+    cdef cnp.uint16_t[::1] index = None
+    cdef cnp.uint16_t n_unique = 0
+
     if return_mapping:
         mapping = np.empty(n, dtype=np.uint16)
     if return_index:
         index = np.empty(n, dtype=np.uint16)
 
-    for i in range(n):
-        pass_all = 1
-        a = vertices[i, 0]
-        b = vertices[i, 1]
-        c = vertices[i, 2]
-        # Check all other accepted vertices for similarity to this one
-        for j in range(n_unique):
-            sim = (a * unique_vertices[j, 0] +
-                   b * unique_vertices[j, 1] +
-                   c * unique_vertices[j, 2])
-            if remove_antipodal:
-                sim = fabs(sim)
-            if sim > cos_similarity:  # too similar, drop
-                pass_all = 0
-                if return_mapping:
-                    mapping[i] = j
-                # This point unique_vertices[j] already has an entry in index,
-                # so we do not need to update.
-                break
-        if pass_all:  # none similar, keep
-            unique_vertices[n_unique, 0] = a
-            unique_vertices[n_unique, 1] = b
-            unique_vertices[n_unique, 2] = c
-            if return_mapping:
-                mapping[i] = n_unique
-            if return_index:
-                index[n_unique] = i
-            n_unique += 1
+    # Call the optimized Cython function
+    remove_similar_vertices_c(
+        vertices, theta, remove_antipodal,
+        return_mapping, return_index,
+        unique_vertices, mapping, index, &n_unique
+    )
 
-    verts = unique_vertices[:n_unique].copy()
+    # Prepare the outputs
+    verts = np.asarray(unique_vertices[:n_unique]).copy()
     if not return_mapping and not return_index:
         return verts
+
     out = [verts]
     if return_mapping:
-        out.append(mapping)
+        out.append(np.asarray(mapping))
     if return_index:
-        out.append(index[:n_unique].copy())
+        out.append(np.asarray(index[:n_unique]).copy())
     return out
 
 
+@cython.boundscheck(False)
+@cython.wraparound(False)
+cdef cnp.npy_intp search_descending_c(cython.floating* arr, cnp.npy_intp size, double relative_threshold) noexcept nogil:
+    """
+    Optimized Cython version of the search_descending function.
+
+    Parameters
+    ----------
+    arr : floating*
+        1D contiguous array assumed to be sorted in descending order.
+    size : cnp.npy_intp
+        Number of elements in the array.
+    relative_threshold : double
+        Threshold factor to determine the cutoff index.
+
+    Returns
+    -------
+    cnp.npy_intp
+        Largest index `i` such that all(arr[:i] >= T), where T = arr[0] * relative_threshold.
+    """
+    cdef:
+        cnp.npy_intp left = 0
+        cnp.npy_intp right = size
+        cnp.npy_intp mid
+        double threshold
+
+    # Handle edge case of empty array
+    if right == 0:
+        return 0
+
+    threshold = relative_threshold * arr[0]
+
+    # Binary search for the threshold
+    while left != right:
+        mid = (left + right) // 2
+        if arr[mid] >= threshold:
+            left = mid + 1
+        else:
+            right = mid
+
+    return left
+
 @cython.boundscheck(False)
 @cython.wraparound(False)
 def search_descending(cython.floating[::1] a, double relative_threshold):
@@ -184,27 +258,37 @@ def search_descending(cython.floating[::1] a, double relative_threshold):
     10
 
     """
-    if a.shape[0] == 0:
-        return 0
+    return search_descending_c(&a[0], a.shape[0], relative_threshold)
 
+
+@cython.wraparound(False)
+@cython.boundscheck(False)
+cdef long local_maxima_c(double[:] odf, cnp.uint16_t[:, :] edges, double[::1] out_values,
+        cnp.npy_intp[::1] out_indices) noexcept nogil:
     cdef:
-        cnp.npy_intp left = 0
-        cnp.npy_intp right = a.shape[0]
-        cnp.npy_intp mid
-        double threshold = relative_threshold * a[0]
-
-    while left != right:
-        mid = (left + right) // 2
-        if a[mid] >= threshold:
-            left = mid + 1
-        else:
-            right = mid
-    return left
+        long count
+        cnp.npy_intp* wpeak = <cnp.npy_intp*>malloc(odf.shape[0] * sizeof(cnp.npy_intp))
 
+    if not wpeak:
+        return -3  # Memory allocation failed
+
+    memset(wpeak, 0, odf.shape[0] * sizeof(cnp.npy_intp))
+    count = _compare_neighbors(odf, edges, wpeak)
+    if count < 0:
+        free(wpeak)
+        return count
+
+    memcpy(&out_indices[0], wpeak, count * sizeof(cnp.npy_intp))
+    take(&odf[0], &out_indices[0], <int>count, &out_values[0])
+
+    _cosort(out_values, out_indices)
+
+    free(wpeak)
+
+    return count
 
 @cython.wraparound(False)
 @cython.boundscheck(False)
-@cython.profile(True)
 def local_maxima(double[:] odf, cnp.uint16_t[:, :] edges):
     """Local maxima of a function evaluated on a discrete set of points.
 
@@ -241,17 +325,25 @@ def local_maxima(double[:] odf, cnp.uint16_t[:, :] edges):
     """
     cdef:
         cnp.ndarray[cnp.npy_intp] wpeak
-    wpeak = np.zeros((odf.shape[0],), dtype=np.intp)
-    count = _compare_neighbors(odf, edges, &wpeak[0])
+        double[::1] out_values
+        cnp.npy_intp[::1] out_indices
+
+    out_values = np.zeros(odf.shape[0], dtype=float)
+    out_indices = np.zeros(odf.shape[0], dtype=np.intp)
+
+    count = local_maxima_c(odf, edges, out_values, out_indices)
+
     if count == -1:
         raise IndexError("Values in edges must be < len(odf)")
     elif count == -2:
-        raise ValueError("odf can not have nans")
-    indices = wpeak[:count].copy()
-    # Get peak values return
-    values = np.take(odf, indices)
-    # Sort both values and indices
-    _cosort(values, indices)
+        raise ValueError("odf cannot have NaNs")
+    elif count == -3:
+        raise MemoryError("Memory allocation failed")
+
+    # Wrap the pointers as NumPy arrays
+    values = np.asarray(out_values[:count])
+    indices = np.asarray(out_indices[:count])
+
     return values, indices
 
 
diff --git a/dipy/utils/fast_numpy.pxd b/dipy/utils/fast_numpy.pxd
index 26f5879..efbef04 100644
--- a/dipy/utils/fast_numpy.pxd
+++ b/dipy/utils/fast_numpy.pxd
@@ -8,6 +8,13 @@ from libc.stdlib cimport rand, srand, RAND_MAX
 from libc.math cimport sqrt
 
 
+cdef void take(
+        double* odf,
+        cnp.npy_intp* indices,
+        cnp.npy_intp n_indices,
+        double* values_out) noexcept nogil
+
+
 # Replaces a numpy.searchsorted(arr, number, 'right')
 cdef int where_to_insert(
         cnp.float_t* arr,
diff --git a/dipy/utils/fast_numpy.pyx b/dipy/utils/fast_numpy.pyx
index 57cabc1..69a6750 100644
--- a/dipy/utils/fast_numpy.pyx
+++ b/dipy/utils/fast_numpy.pyx
@@ -2,6 +2,31 @@
 # cython: initializedcheck=False
 # cython: wraparound=False
 from libc.stdio cimport printf
+cimport numpy as cnp
+
+
+cdef void take(
+    double* odf,
+    cnp.npy_intp* indices,
+    cnp.npy_intp n_indices,
+    double* values_out) noexcept nogil:
+    """
+    Mimic np.take(odf, indices) in Cython using pointers.
+
+    Parameters
+    ----------
+    odf : double*
+        Pointer to the input array from which values are taken.
+    indices : npy_intp*
+        Pointer to the array of indices specifying which elements to take.
+    n_indices : npy_intp
+        Number of indices to process.
+    values_out : double*
+        Pointer to the output array where the selected values will be stored.
+    """
+    cdef int i
+    for i in range(n_indices):
+        values_out[i] = odf[indices[i]]
 
 
 cdef int where_to_insert(cnp.float_t* arr, cnp.float_t number, int size) noexcept nogil:
diff --git a/dipy/utils/tests/test_fast_numpy.pyx b/dipy/utils/tests/test_fast_numpy.pyx
index 93bf433..88ee9e7 100644
--- a/dipy/utils/tests/test_fast_numpy.pyx
+++ b/dipy/utils/tests/test_fast_numpy.pyx
@@ -1,11 +1,12 @@
 import timeit
 
+cimport numpy as cnp
 import numpy as np
 from numpy.testing import (assert_, assert_almost_equal, assert_raises,
                             assert_array_equal)
 from dipy.utils.fast_numpy import random, seed, random_point_within_circle
 from dipy.utils.fast_numpy cimport (norm, normalize, dot, cross, random_vector,
-                                    random_perpendicular_vector)
+                                    random_perpendicular_vector, take)
 
 
 def test_norm():
@@ -125,3 +126,13 @@ def test_random_point_within_circle():
     for r in np.arange(0, 5, 0.2):
         pts = random_point_within_circle(r)
         assert_(np.linalg.norm(pts) <= r)
+
+
+def test_take():
+    # Test that the take function is equal to numpy.take
+    cdef int n_indices = 5
+    cdef double[:] odf = np.random.random(10)
+    cdef cnp.npy_intp[:] indices = np.random.randint(0, 10, n_indices, dtype=np.intp)
+    cdef double[:] values_out = np.zeros(n_indices, dtype=float)
+    take(&odf[0], &indices[0], n_indices, &values_out[0])
+    assert_array_equal(values_out, np.take(odf, indices))
\ No newline at end of file
